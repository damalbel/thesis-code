cdef extern from "fmpc.h":
    void mpc(double* A,    double* B,    double* Q,     double* R, double* xmax, double* xmin,
			   double* umax, double* umin, int     n,     int m,
			   int     T,    double* Qf,   double  kappa, int niter, int quiet,
			   double* X0,   double* U0,   double* x0,
			   double* X,     double* U,    double  telapsed);

