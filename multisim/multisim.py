# distribution simulator
import numpy as np
import cmath
from numpy.linalg import inv, det
from scipy import optimize
from termcolor import colored, cprint

from generators import *

import matplotlib.pyplot as plt

# macros

# helper functions (move this to a module)

def expandComplex(i, j, a, b, mat):
    mat[2*i    , 2*j    ]     += a
    mat[2*i + 1, 2*j + 1]     += a
    mat[2*i    , 2*j + 1]     += -b
    mat[2*i + 1, 2*j    ]     += b

def rotor2net(delta, rvec):
    nvec = np.zeros(2)
    nvec[0] =  np.sin(delta)*rvec[0] + np.cos(delta)*rvec[1];
    nvec[1] = -np.cos(delta)*rvec[0] + np.sin(delta)*rvec[1];

    return nvec

def net2rotor(delta, nvec):
    rvec = np.zeros(2)
    rvec[0] = np.sin(delta)*nvec[0] - np.cos(delta)*nvec[1];
    rvec[1] = np.cos(delta)*nvec[0] + np.sin(delta)*nvec[1];

    return rvec

# class definitions
class Bus(object):
    """ Generic bus class """
    def __init__(self, n):
	self.n = n

    def setvinit(self, vinit):
        self.v0 = vinit

class Branch(object):
    """ Generic branch class """
    def __init__(self, i, j, r, x, sh = 0.0):
	self.fr = i
	self.to = j
	self.r  = r # resistance (p.u)
        self.x  = x # reactance (p.u)
        self.sh = sh # shunt reactance (p.u)

class ZLoad(object):
    """ Generic Z Load class """
    def __init__(self, bus, z):
        self.bus = bus
        self.yre = z.real
        self.yim = z.imag

    def injcurrent(self, v, curr):
        """ returns a vector with injected current at bus.
        The input v is an array such as v[0] = vre, v[1] = vim """
        curr[2*self.bus    ] = v[0]*self.yre - v[1]*self.yim
        curr[2*self.bus + 1] = v[0]*self.yim + v[1]*self.yre

# system class: this class is composed by buses, branches, etcc
# and has methods to modify Ybus etc..

class System(object):

    def __init__(self):
        self.nbuses    = 0
        self.nbranches = 0
        self.nloads    = 0
        self.nevents   = 0

        self.events    = []
        self.buses     = []
        self.branches  = []
        self.loads     = []

        # options flags
        self.verbose = False
        self.pmsg = True


        # Devices are those elements external
        # to the admittance matrix
        self.devices            = []
        self.num_devices        = 0
        self.num_dof_alg        = 0
        self.num_dof_dif        = 0

    def silent(self):
        self.pmsg = False
        self.verbose   = False

    def verbose(self):
        self.verbose = True

    def adddevice(self, device):
        self.devices.append(device)
        self.devices[-1].setpointers(self.num_dof_dif, self.num_dof_alg, self.num_devices)
        dif, alg = self.devices[-1].getdim()
        self.num_devices += 1
        self.num_dof_alg += alg
        self.num_dof_dif += dif

    def addload(self, load):
        self.loads.append(load)
        self.nloads += 1
    
    def addbranch(self, i, j, r, x, sh = 0.0):
        self.branches.append(Branch(i, j, r, x, sh = 0.0))
        self.nbranches += 1
    
    def addbus(self, n):
        self.buses.append(Bus(n))
        self.nbuses += 1
    
    def createYbusComplex(self):
        """ Create Ybus matrix from bus and branch data """

        dim  = len(self.buses)
        self.ybus = np.zeros((dim, dim), dtype=complex)

        for branch in self.branches:
	    fr = branch.fr
	    to = branch.to
	    y  = (1.0/(branch.r + 1j*branch.x))
	    self.ybus[fr,fr] += y
	    self.ybus[to,to] += y
	    self.ybus[fr,to] -= y
	    self.ybus[to,fr] -= y
    
    def createYbusReal(self):
        """ Create Ybus matrix from bus and branch data
         using an homomorphism in R^2 to avoid complex
         algebra """

        dim  = len(self.buses)
        self.ybus = np.zeros((2*dim, 2*dim))

        for branch in self.branches:
	    fr = branch.fr
	    to = branch.to
            y  = (1.0/(branch.r + 1j*branch.x))
            a  = np.real(y)
            b  = np.imag(y)
            
            expandComplex(fr, fr, a, b, self.ybus)
            expandComplex(to, to, a, b, self.ybus)
            expandComplex(fr, to, -a, -b, self.ybus)
            expandComplex(to, fr, -a, -b, self.ybus)

    def applyBusFault(self, busn, z):
        """
        Name: applyBusFault
        Description: apply a fault of impedance z at bus 'busn'

        INPUTS:
        1. busn :: int, bus number
        2. z    :: complex, fault impedance

        OUTPUT:
        1. fid  :: int, fault ID
        """

        print colored(">>>APPLYING FAULT", 'red') + " at bus: %d, z: %g." % (busn, z)
        self.events.append(BusFault(busn, z, self.nevents))
        self.nevents += 1

        y = 1/z
        expandComplex(busn, busn, y.real, y.imag, self.ybus)

        return self.events[-1].event_id
    
    def removeBusFault(self, faultid):
        """
        Name: removeBusFault
        Description: remove previous applied fault

        INPUTS:
        1. faultid :: int, fault id
        """

        busn = self.events[faultid].busn
        z    = self.events[faultid].z
        print colored("<<<REMOVING FAULT", 'red') + " at bus: %d, z: %g." % (busn, z)
        self.nevents -= 1
        y = 1/z
        expandComplex(busn, busn, -y.real, -y.imag, self.ybus)

        return None

    def voltIdx(self, bus):
        """
        Return pointer to bus voltage in global array.
        """
        return self.num_dof_alg + self.num_dof_dif + 2*bus

class Event(object):

    def __init__(self, event_id):
        self.event_id = event_id

    def end(self):
        self.event_id = -1

class BusFault(Event):

    def __init__(self, busn, z, event_id):
        self.busn = busn
        self.z    = z
        Event.__init__(self, event_id)

    def end(self):
        Event.end(self)

def initialize_system(system):

    alg_size = system.num_dof_alg
    dif_size = system.num_dof_dif
    cur_size = 2*system.nbuses #current balance size or dim(Ybus)
    sys_size = alg_size + dif_size + 2*system.nbuses

    sysvec   = np.zeros(sys_size)
    x        = np.zeros(dif_size)  #differential part
    y        = np.zeros(alg_size)  #algebraic part
    v        = np.zeros(cur_size) # voltage vector

    for i in range(system.nbuses):
        v[2*i]     = system.buses[i].v0.real
        v[2*i + 1] = system.buses[i].v0.imag
    
    for device in system.devices:
        device.initialize(v[2*device.bus], v[2*device.bus + 1], x, y)
 
    sysvec[:dif_size]  = x
    sysvec[dif_size:dif_size + alg_size]  = y
    sysvec[dif_size + alg_size:]  = v
    
    return sysvec

def form_residual_beuler(x, system, x0, h):
    """
    residual r  = [q g]'

    where g is the algebraic residual f(x) and q the differential,
    in the case of the euler integration method, defined by:

    q = x_{t + 1} - x_{t} - h*f(t + 1)

    """

    alg_size = system.num_dof_alg
    dif_size = system.num_dof_dif
    cur_size = 2*system.nbuses #current balance size or dim(Ybus)
    sys_size = alg_size + dif_size + 2*system.nbuses

    r     = np.zeros(sys_size)
    q     = np.zeros(dif_size)  #differential part
    f     = np.zeros(dif_size)  #differential part
    g     = np.zeros(alg_size + cur_size)  #algebraic part

    idev  = np.zeros(cur_size)
    iload = np.zeros(cur_size)
    inet  = np.zeros(cur_size)

    # divide x vector in differential and algebraic parts
    x_dif = x[:dif_size]
    x_alg = x[dif_size:]
    v     = x[alg_size + dif_size:]

    # here elements will write in f and g

    for device in system.devices:
        bus = device.bus
        device.residualDiff(x_dif, x_alg, f)
        device.residualAlg(x_dif, x_alg, v[2*bus:], g)
    
    # differential part integration
    q  = x[:dif_size] - x0[:dif_size] - h*f
    # current balance equations
    for device in system.devices:
        device.injectedCurrents(x_dif, x_alg, idev)
    
    for load in system.loads:
        bus = load.bus
        load.injcurrent(v[2*bus:], iload)
    
    inet  = np.dot(system.ybus, x[dif_size + alg_size:])
    
    g[alg_size:]  = -idev+inet+iload

    r[:dif_size]  = q
    r[dif_size:]  = g


    return r


def integrate_system(x, system, time, trajectory = None):

    """
    Name: integrate_system
    Description: Integrates the system from time[0] to time[end]

    INPUTS:
    1.- x: initial trajectory strate vector
    2.- system: system data
    3.- time: an array with all the time points. the time step is calculated 
        at each time points.
    4.- trajectory (optional): array to store trajectory

    OUTPUTS:
    1. np.array: solution vector at final time step
    """

    for i in xrange(len(time) -1):
        if system.pmsg:
            print "Propagating to t = " + colored(str(time[i + 1]), 'green')   
        sol = optimize.root(form_residual_beuler, x, args = (system, x, time[i + 1] - time[i]), method = 'krylov', options={'xtol': 1e-8, 'disp': system.verbose})

        x = sol.x
        if trajectory is not None:
            trajectory[:,i] = np.copy(x)

    return x

def propagate_system(x, system, step):

    """
    Name: propagate_system
    Description: propagates the system from t = 0 to t = step. Similar to integrate_system
	but only integrates to the next time step.

    INPUTS:
    1.- x: initial trajectory strate vector
    2.- system: system data
    3.- step: integration step

    OUTPUTS:
    1. np.array: solution vector at final time step
    """
    sol = optimize.root(form_residual_beuler, x, args = (system, x, step), method = 'krylov', options={'xtol': 1e-8, 'disp': system.verbose})
    x = sol.x

    return x

def solve_steady_system(x, system):

    """
    Name: solve_steady_system
    Description:

    INPUTS:
    1.- x: initial guess
    2.- system, generators, loads: system data (merge everything into system)
        at each time points.

    OUTPUTS:
    1. np.array solution vector at final time step
    """
    
    print colored('Solve steady state', 'green')   
    sol = optimize.root(form_residual_beuler, x, args = (system, x, 0.0), method = 'krylov',options={'xtol': 1e-8, 'disp': system.verbose})

    x = sol.x

    return x

def generator_plots(system, trajectory, bus):

    for device in system.devices:
        if (device.bus == bus and device.model_type == 'generator'):
            for i in range(device.dif_dim):
                plt.figure()
                plt.plot(trajectory[device.dif_ptr + i,:])
                plt.title("Generator at bus %d. (Device: %d)" % (device.bus, device.ndev))
                plt.ylabel(device.state_list[i])
                plt.xlabel("Time (sec)")

