from distutils.core import setup, Extension
from Cython.Build import cythonize

import numpy

ext = Extension("mpc", libraries=['blas','lapack'],
                sources=["mpc.pyx", "fmpc.c"],
                include_dirs=[numpy.get_include()])

setup(name="mpc",
      ext_modules = cythonize([ext]))
