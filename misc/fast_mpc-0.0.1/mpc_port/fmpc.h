/*
 * Function name: mpc
 * Description:
 * Inputs:
 *
 *   SYSTEM
 * - double *A       :   dynamics matrix
 * - double *B       :   input matrix
 * - double *Q       :   state cost matrix
 * - double *R       :   state input matrix
 * - double *xmax    :   state upper limits x_{max}
 * - double *xmin    :   state lower limits x_{min}
 * - double *umax    :   input upper limits u_{max}
 * - double *umin    :   input lower limits u_{min}
 * - int    n        :   number of states.
 * - int    m        :   number of inputs
 *
 *   PROBLEM
 * - int     T        :   mpc horizon
 * - double* Qf       :   terminal cost
 * - double  kappa    :   barrier parameter
 * - int     niter    :   number of newton iterations
 * - int     quiet    :   quiet?
 *
 *   OPTIONAL
 * - double *X0       :   warm start state trajectory
 * - double *U0       :   ""         input
 * - double *x0       :   initial state
 *
 *
 * Outputs:
 *
 * - double  *X       :   optimal state trajectory
 * - double  *U       :   optimal input trajectory
 * - double   telapsed :   time elapsed
 */



void mpc(double* A,    double* B,    double* Q,     double* R, double* xmax, double* xmin,
			   double* umax, double* umin, int     n,     int m,
			   int     T,    double* Qf,   double  kappa, int niter, int quiet,
			   double* X0,   double* U0,   double* x0,
				 double* X,     double* U,    double  telapsed);
