import numpy as np
import mpc


n = 12 # state dimension
m = 3  # input dimension

T      = 10
kappa  = 0.01
niter = 5
quiet  = 0


A = np.fromfile('A.bin', dtype = 'Float64').reshape((12, 12), order = 'F')
B = np.fromfile('B.bin', dtype = 'Float64').reshape((12, 3), order='F')

A = np.asfortranarray(A)
B = np.asfortranarray(B)

print B

Q = np.asfortranarray(np.eye(n))     
R = np.asfortranarray(np.eye(m))       

x0   = np.zeros(n)
xmin = -4*np.ones(n)
xmax =  4*np.ones(n)
umin = -0.5*np.ones(m)
umax = 0.5*np.ones(m)

Qf     = Q

X = np.zeros((n,T), order = 'F')
U = np.zeros((m,T), order = 'F')
x = x0

X0 = np.zeros((n,T), order = 'F')
U0 = np.zeros((m,T), order = 'F')

mpc.mpc_step(A, B, Q, R, xmax, xmin, umax, umin, n, m, T, Qf, kappa, niter, quiet, X0, U0, x, X, U, 0.1)
x = np.array([0, 0, 0, 0, 0, 0, -0.4950, -0.4950, -0.4950, -0.4950, -0.4950, -0.4950], dtype = np.float64)

mpc.mpc_step(A, B, Q, R, xmax, xmin, umax, umin, n, m, T, Qf, kappa, niter, quiet, X0, U0, x, X, U, 0.1)


print U
