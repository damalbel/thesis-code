#: sources=["fmpc.c"]
cimport mpc

import cython

# import both numpy and the Cython declarations for numpy
import numpy as np
cimport numpy as np


# declare the interface to the C code


@cython.boundscheck(False)
@cython.wraparound(False)
def mpc_step(np.ndarray[double, ndim=2, mode="fortran"] A not None,
             np.ndarray[double, ndim=2, mode="fortran"] B not None,
             np.ndarray[double, ndim=2, mode="fortran"] Q not None,
             np.ndarray[double, ndim=2, mode="fortran"] R not None,
             np.ndarray[double, ndim=1, mode="c"] xmax not None,
             np.ndarray[double, ndim=1, mode="c"] xmin not None,
             np.ndarray[double, ndim=1, mode="c"] umax not None,
             np.ndarray[double, ndim=1, mode="c"] umin not None,
             int n, int m, int T,
             np.ndarray[double, ndim=2, mode="fortran"] Qf not None,
             double kappa, int niter, int quiet,
             np.ndarray[double, ndim=2, mode="fortran"] X0 not None,
             np.ndarray[double, ndim=2, mode="fortran"] U0 not None,
             np.ndarray[double, ndim=1, mode="c"] x0 not None,
             np.ndarray[double, ndim=2, mode="fortran"] X not None,
             np.ndarray[double, ndim=2, mode="fortran"] U not None, double telapsed):
    """
    void mpc(double* A,    double* B,    double* Q,     double* R, double* xmax, double* xmin,
			   double* umax, double* umin, int     n,     int m,
			   int     T,    double* Qf,   double  kappa, int niter, int quiet,
			   double* X0,   double* U0,   double* x0,
			   double* X     double* U,    double  telapsed);
    """
    
    cdef double[::1] p_xmax = xmax
    cdef double[::1] p_xmin = xmin
    cdef double[::1] p_umax = umax
    cdef double[::1] p_umin = umin
    cdef double[::1] p_x0   = x0
    
    cdef double[:,:] p_A = A
    cdef double[:,:] p_B = B
    cdef double[:,:] p_Q = Q
    cdef double[:,:] p_R = R
    cdef double[:,:] p_Qf = Qf
    cdef double[:,:] p_X0 = X0
    cdef double[:,:] p_U0 = U0
    cdef double[:,:] p_X = X
    cdef double[:,:] p_U = U

    mpc(&p_A[0,0], &p_B[0,0], &p_Q[0,0], &p_R[0,0], &p_xmax[0], &p_xmin[0], 
            &p_umax[0], &p_umin[0], n, m, T, &p_Qf[0,0], kappa, niter, quiet, 
            &p_X0[0,0], &p_U0[0,0], &p_x0[0], &p_X[0,0], &p_U[0, 0], telapsed)

    return None

