#include <stdio.h>
#include <stdlib.h>
#include "blas.h"

void matrix(int a) {

  int     b, i, max;
  double  *m;

  b = 1;
  m = calloc(a, sizeof(double));

  for (i = 0; i < a; i++) {
    m[i] = (double)i;
  }

  printf("cacota de vaca.\n");
  max = F77_NAME(idamax)(&a, m, &b);
  printf("Max is %d.\n", max);

  free(m);
}


void vec_sum(double *vec, int n) {

  int    i;
  double result_temp = 0;

  for (i = 0; i < n; i++) {
    result_temp += vec[i];
  }

  printf("The sum of the vector of size %d is %f.\n", n, result_temp);
  printf("Now I change the first entry to 100\n");
  vec[0] = 100.0;
}

