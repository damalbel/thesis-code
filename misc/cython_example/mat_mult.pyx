#: sources=["mmult.c"]
cimport mat

import cython

# import both numpy and the Cython declarations for numpy
import numpy as np
cimport numpy as np


def matrix(int a):
    mat.matrix(a)



# declare the interface to the C code
#cdef extern void vec_sum (double* array, int n)

@cython.boundscheck(False)
@cython.wraparound(False)
def vec_sum(np.ndarray[double, ndim=1, mode="c"] input not None):
    """
    multiply (arr, value)

    Takes a numpy arry as input, and multiplies each elemetn by value, in place

    param: array -- a 2-d numpy array of np.float64
    param: value -- a number that will be multiplied by each element in the array

    """
    cdef int n

    n = input.shape[0]
    cdef double[::1] array = input

    mat.vec_sum(&array[0], n)

    return None

