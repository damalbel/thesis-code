from distutils.core import setup, Extension
from Cython.Build import cythonize

import numpy

ext = Extension("mat_mult", libraries=['blas'],
                sources=["mat_mult.pyx", "mmult.c"],
                include_dirs=[numpy.get_include()])

setup(name="matrix",
      ext_modules = cythonize([ext]))
