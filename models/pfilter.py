import numpy as np
from numpy import linalg as LA
from scipy.stats import norm

# This module implements a set of tools for particle filtering
#
# Originally this was written in a more "object oriented" fashio, but since:
#  a) I'm not an expert on OO design
#  b) Ultimately I'll delegate stuff to C code
#  I've decided to just write a collection of functions and use classes just
#  as a "C struct"

class cloud(object):
    """ Implements a cloud of particles """

    def __init__(self, npart, dim, odim = 1):
        self.n = npart
        self.dim = dim
        self.w   = (1.0/self.n)*np.ones(npart)
        self.x   = np.zeros((dim, npart))
        self.y   = np.zeros((odim, npart))

    def pprint(self):
        for i in range(self.n):
            print str(i) + ' x: ' + str(self.x[:,i]) + ' w: ' + str(self.w[i])

def normalize(w):
    """ Input:  a numpy array of particle weights
        Output: normalized array 
    """
    tw =  np.sum(w)
    w  *= (1.0/tw)


def neff(w):
    """ Input:  a numpy array of particle weights
        Output: neff index
    """
    return 1.0/(LA.norm(w, 2)**2)


def resample_multinomial(w, x):
    """
        Implements multinomial resampling. This is an expensive function,
        might consider wrapping a C function or use cython.
    """
    dim, n = x.shape
    invn   = 1.0/n
    c      = np.cumsum(w)
    xnew   = np.zeros((dim, n))

    u0 = np.random.uniform(0.0, invn, 1)[0]

    for j in range(n):

        u = u0 + invn*j
        i = 0

        while (u > c[i]):
            i += 1;

        xnew[:,j] = x[:,i]

    return xnew

def resample_systematic(w, x):
    """
        Implements systematic resampling. This is an expensive function,
        might consider wrapping a C function or use cython.
    """
    dim, n = x.shape
    xnew   = np.zeros((dim, n))

    u = np.random.uniform()/float(n)

    s = 0.0
    j = 0

    for i in range(n):
        
        s += w[i]

        while (u <= s and j < n):

            xnew[:,j] = x[:,i]
            j += 1
            u += 1.0/float(n)

        if (j == n): break

    return xnew

if __name__ == "__main__":

    print "Particle filter test"
    cl = cloud(10, 2)
    
    # dummy data
    for i in range(cl.n):
        cl.w[i] = (i + 1)**2
        cl.x[:,i] = i

    normalize(cl.w)
    cl.pprint()

    print "Neff: " + str(neff(cl.w))

    print "Resampling multinomial"
    cl.x = resample_multinomial(cl.w, cl.x)
    cl.pprint()
    
    print "Resampling systematic"
    cl.x = resample_systematic(cl.w, cl.x)
    cl.pprint()

