# Comparison of initialization and dynamics of simplified and higher order motor.

import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt

ws = 2*np.pi*60

class cim5(object):

    """ Implementation of cim5. single cage for now """

    def __init__(self, ra, xa, xm, r1, x1, h, d):
        
        self.ra     = ra
        self.xa     = xa
        self.xm     = xm
        self.r1     = r1
        self.x1     = x1
        self.h      = h
        self.d      = d
        self.t_m    = -1

        self.tp     = (x1 + xm)/(r1*ws)
        self.x0     = (xa + xm)
        self.x_p    = xa + (x1*xm)/(x1 + xm)


    def initialize(self, x, v_ds, v_qs, pelec):

        F    = np.zeros(6)

        e_dp = x[0]
        e_qp = x[1]
        s    = x[2]
        i_ds = x[3]
        i_qs = x[4]
        t_m  = x[5]

        F[0] = (-1.0/self.tp)*(e_dp + (self.x0 - self.x_p)*i_qs) + s*ws*e_qp
        F[1] = (-1.0/self.tp)*(e_qp - (self.x0 - self.x_p)*i_ds) - s*ws*e_dp

        F[2] = (1.0/(2.0*self.h))*(t_m - e_dp*i_ds - e_qp*i_qs)
        
        F[3] = self.ra*i_ds - self.x_p*i_qs + e_dp - v_ds
        F[4] = self.ra*i_qs + self.x_p*i_ds + e_qp - v_qs

        F[5] = v_ds*i_ds + v_qs*i_qs - pelec
 
        return F
    
    def propagate(self, h, xold, xnew, v_ds, v_qs):


        e_dp = xold[0]
        e_qp = xold[1]
        s    = xold[2]
        i_ds = xold[3]
        i_qs = xold[4]

        xnew[0] = e_dp + h*((-1.0/self.tp)*(e_dp + (self.x0 - self.x_p)*i_qs) + s*ws*e_qp)
        xnew[1] = e_qp + h*((-1.0/self.tp)*(e_qp - (self.x0 - self.x_p)*i_ds) - s*ws*e_dp)
        xnew[2] = s + h*((1.0/(2.0*self.h))*(self.t_m - e_dp*i_ds - e_qp*i_qs))
        
        #auxiliary calculations
        diff1 = v_ds - xnew[0]
        diff2 = v_qs - xnew[1]
        den   = self.ra**2 + self.x_p**2
        
        xnew[3] = (self.ra*diff1 + self.x_p*diff2)/den
        xnew[4] = (self.ra*diff2 - self.x_p*diff1)/den

class mech(object):

    def __init__(self, ra, xa, xm, r1, x1, h):
        
        self.ra     = ra
        self.xa     = xa
        self.xm     = xm
        self.r1     = r1
        self.x1     = x1
        self.h      = h
        self.t_m    = -1

    def p(self, vm, s):

        num = (self.ra + (self.r1/s))*(vm**2)
        den = (self.ra + (self.r1/s))**2 + (self.xa + self.x1)**2

        return -(num/den)
    
    def q(self, vm, s):

        num = (self.xa + (self.x1/s))*(vm**2)
        den = (self.ra + (self.r1/s))**2 + (self.xa + self.x1)**2

        return (-(vm**2)/self.xm - (num/den))

    def telec(self, vm, s):
        
        num = (self.r1/s)*(vm**2)
        den = (self.ra + (self.r1/s))**2 + (self.xa + self.x1)**2
        
        return num/den

    def zeq(self, s):
        """ Equivalent impedance seen from bus"""
        z2  = self.ra + self.r1/s + 1j*(self.xa + self.x1)
        zeq = ((1j*self.xm)*z2)/((1j*self.xm) + z2)
        return zeq

    def initialize(self, x, v_ds, v_qs, pelec):

        F = np.zeros(2)

        s   = x[0]
        t_m = x[1]

        vm   = np.sqrt(v_ds**2.0 + v_qs**2.0)
        F[0] = t_m - self.telec(vm, s)
        F[1] = self.p(vm, s) - pelec

        return F

    def propagate(self, h, xold, xnew, v_ds, v_qs):

        s   = xold[0]

        vm      = np.sqrt(v_ds**2.0 + v_qs**2.0)

        xnew[0] = s + h*(1/(2.0*self.h))*(self.t_m - self.telec(vm, s))
        
        current = (1.0/self.zeq(xnew[0]))*(v_ds + 1j*v_qs)
        xnew[1] = np.real(current)
        xnew[2] = np.imag(current)




if __name__ == "__main__":

    motor1 = cim5(0.0138, 0.083, 3.0, 0.0550, 0.053, 1.0, 0.0)

    v_ds = 1.0144
    v_qs = -0.0589
    pelec  = 1.06484


    steps = 1000
    time  = np.linspace(0,5,steps)
    volt  = np.zeros((2, steps))

    for i in range(len(time)):
        if (time[i] > 0.1) and (time[i] < 0.3):
            volt[0,i] = v_ds * 0.6
            volt[1,i] = v_qs * 0.6
        else:
            volt[0, i] = v_ds
            volt[1, i] = v_qs

    x0  = np.ones(6)
    print "Initializing machine cim5 with vds: %g, vqs: %g, p: %g" % (v_ds, v_qs, pelec)
    sol = optimize.root(motor1.initialize, x0, args = (v_ds, v_qs, pelec))
    print "\tEdp:   " + str(sol.x[0])
    print "\tEqp:   " + str(sol.x[1])
    print "\ts:     " + str(sol.x[2])
    print "\ti_ds:  " + str(sol.x[3])
    print "\ti_qs:  " + str(sol.x[4])
    print "\tt_m:   " + str(sol.x[5])
    qelec = v_qs*sol.x[3] - v_ds*sol.x[4]
    print "\tqelec: " + str(qelec)
    
    print "Integrating machine cim5 from t = %g to t = %g with a step: %g" % (time[0],
            time[-1], time[1])

    x_motor1 = np.zeros((5, steps))
    x_motor1[:,0] = sol.x[0:5]
    
    motor1.t_m = sol.x[5]

    for i in range(len(time) - 1):
        motor1.propagate(time[1], x_motor1[:, i], x_motor1[:, i + 1], volt[0, i + 1],
                volt[1, i + 1])
    

    plt.plot(time, x_motor1[2,:])

    motor2 = mech(0.0138, 0.083, 3.0, 0.0550, 0.053, 1.0)
    x0  = np.ones(2)
    x0[0] = 0.1 #initial guess so N-R converges
    print "\n\nInitializing machine 'mech' with vds: %g, vqs: %g, p: %g" % (v_ds, v_qs, pelec)
    sol = optimize.root(motor2.initialize, x0, args = (v_ds, v_qs, pelec))
    print "s: " + str(sol.x[0])
    print "t_m: " + str(sol.x[1])
    
    vm      = np.sqrt(v_ds**2.0 + v_qs**2.0)
    print "p_e: " + str(motor2.p(vm, sol.x[0]))
    print "q_e: " + str(motor2.q(vm, sol.x[0]))
    
    print "Integrating machine 'mech' from t = %g to t = %g with a step: %g" % (time[0],
            time[-1], time[1])

    x_motor2 = np.zeros((3, steps))
    x_motor2[:,0] = sol.x[0:1]
    
    motor2.t_m = sol.x[1]

    for i in range(len(time) - 1):
        motor2.propagate(time[1], x_motor2[:, i], x_motor2[:, i + 1], volt[0, i + 1],
                volt[1, i + 1])

    print x_motor2[:,0:4]

    plt.plot(time, x_motor2[0,:])
    #plt.show()


