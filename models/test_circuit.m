%motor parameters
ra = 0.0138;
xa = 0.083;
xm = 3.0;
r1 = 0.055;
x1 = 0.053;
H  = 1.0;

% initial conditions
v = 1.0144 - 1j*0.0589;
s = -0.0570574550225;
%s = 0.0619638513094;

% circuit a - calculate i and p
z2  = (ra + r1/s) + 1j*(xa + x1);
zeq = (z2*(1j*xm))/(z2 + 1j*xm);

i = v/zeq
p = v*conj(i)

% circuit b - calculate i and p

z2  = (r1/s) + 1j*x1;
za  =  ra    + 1j*xa;
zeq = za + (z2*(1j*xm))/(z2 + 1j*xm);

i = v/zeq
p = v*conj(i)

